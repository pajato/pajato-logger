# Contribution Guide (wip):

## General

All code contributions are expected to be clean code.
The means the code generally adheres to the principles and practices espoused by Uncle Bob (Robert Martin) in his classes, books, blog posts and videos.
For classes, books, blog posts and articles see [cleancoder.com](http://cleancoder.com/products).
For Videos see both [cleancoders.com](https://cleancoders.com) and YouTube where you can find many free talks and presentations.

Clean code for the Argus projects also means the code adheres to the [Kotlin Coding Conventions]().

## Pull Requests (PR)
For a PR to be accepted (merged) the following will be evaluated and considered:

Is the source code clean? Is it spotless? Does it have a bad smell?
- Are there spurious and unnecessary comments?
- Are functions of length ~4 lines long, preferably less?
- Is block structure employed (see Structure and Interpretation fo Computer Programs - SICP, by Abelson & Sussman, MIT Press)
- Does the source code scream "Streaming Video Information"?
- Do names reveal intent?
- Was the code developed using TDD?
- Does the code adhere to the SOLID and Component Principles?

Is the code coverage support 100%?

Does the PR address a bug, issue, etc. identified and possibly discussed on the relevant GitLab project or on the
[Clean Coders Collaborative](https://join.slack.com/t/cleancodersgroup/shared_invite/enQtNzEyMjgyMDU3MDYwLTZkMjViNWVkZTJlNTI4NjZmZGIzNGY5NDA2NThhMGJiOWNmMTRkYmI4ZTUzYzI0YzJmYTZhNDMwM2UzY2I3MjU)
Slack workspace?
