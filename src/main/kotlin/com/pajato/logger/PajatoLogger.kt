package com.pajato.logger

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.logging.ConsoleHandler
import java.util.logging.LogManager
import java.util.logging.Logger

class PajatoLogger(val name: String) {
    init {
        defaultName = name
    }

    companion object {
        var defaultName: String = ""
        var logger: Logger = Logger.getLogger(defaultName)
        fun log(msg: String?, name: String = defaultName) {
            val timestamp: String = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now())
            logger.info("$timestamp [$name] $msg")
        }
    }

    init {
        LogManager.getLogManager().reset()
        logger.addHandler(ConsoleHandler().also { it.formatter = LogFormatter() })
    }
}
