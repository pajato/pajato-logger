package com.pajato.logger

import java.util.logging.Formatter
import java.util.logging.LogRecord

internal class LogFormatter : Formatter() {
    override fun format(record: LogRecord?): String {
        return "${record?.message}\n"
    }
}
