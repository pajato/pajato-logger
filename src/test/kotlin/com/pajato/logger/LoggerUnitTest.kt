package com.pajato.logger

import com.pajato.logger.PajatoLogger.Companion.log
import java.util.logging.Handler
import java.util.logging.LogRecord
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class LoggerUnitTest {
    private lateinit var spy: LoggerSpy
    private val logger = PajatoLogger("test")

    @BeforeTest fun setUp() {
        spy = LoggerSpy()
        PajatoLogger.logger.addHandler(spy)
    }

    @Test fun `When a defaulted log entry is added, verify behavior`() {
        val message = "Something notable happened!"
        val name = "[test]"
        log(message)
        assertTrue(spy.item.contains(message))
        assertTrue(spy.item.contains(name))
        assertEquals("test", logger.name)
    }

    @Test fun `When a named log entry is added, verify behavior`() {
        val message = "Something notable happened!"
        val name: String = this.javaClass.simpleName
        log(message, name)
        assertTrue(spy.item.contains(message))
        assertTrue(spy.item.contains(name))
    }

    class LoggerSpy : Handler() {
        lateinit var item: String

        override fun publish(record: LogRecord?) { item = record!!.message }

        override fun flush() { TODO("Not yet implemented") }

        override fun close() { println("Close executed") }
    }
}
