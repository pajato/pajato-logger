package com.pajato.logger

import java.util.logging.Level
import java.util.logging.LogRecord
import kotlin.test.Test
import kotlin.test.assertEquals

class LogFormatterUnitTest {
    @Test fun `When the input record is null, verify behavior`() {
        val result = LogFormatter().format(null)
        assertEquals("null\n", result)
    }

    @Test fun `When the input record is non-null, verify behavior`() {
        val message = "xyzzy"
        val result = LogFormatter().format(LogRecord(Level.INFO, message))
        assertEquals("$message\n", result)
    }
}
