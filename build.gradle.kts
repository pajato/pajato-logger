import kotlinx.kover.gradle.plugin.dsl.AggregationType.COVERED_PERCENTAGE
import kotlinx.kover.gradle.plugin.dsl.CoverageUnit.INSTRUCTION

plugins {
    kotlin("jvm") version "2.0.20"
    kotlin("plugin.serialization") version "2.0.20"
    id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    id("org.jetbrains.kotlinx.kover") version "0.8.3"
    id("io.github.gradle-nexus.publish-plugin") version "2.0.0"
    signing
    `maven-publish`
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.getByName("main").allSource)
}

val testJar by tasks.registering(Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets.test.get().output)
    exclude("**/*UnitTest*.class")
    exclude("**/*IntegrationTest.class")
    exclude(".lsp")
    exclude("sample_projects")
}

group = "com.pajato"
version = "0.9.14"
description = "The Pajato Logger project"

ktlint {
    additionalEditorconfig.set(
        mapOf(
            "ktlint_standard_function-signature" to "disabled",
            "ktlint_standard_try-catch-finally-spacing" to "disabled",
            "ktlint_standard_statement-wrapping" to "disabled",
            "ktlint_code_style" to "intellij_idea",
        ),
    )
    android.set(false)
    baseline.set(file("my-project-ktlint-baseline.xml"))
    coloredOutput.set(true)
    debug.set(true)
    enableExperimentalRules.set(false)
    ignoreFailures.set(false)
    outputColorName.set("RED")
    outputToConsole.set(true)
    version.set("1.1.1")
    verbose.set(true)
    kotlinScriptAdditionalPaths { include(fileTree("scripts/")) }
    filter {
        exclude("**/generated/**")
        include("**/kotlin/**")
    }
}

kover {
    reports.total.apply {
        html.onCheck.set(true)
        verify.rule {
            bound {
                aggregationForGroup.set(COVERED_PERCENTAGE)
                coverageUnits.set(INSTRUCTION)
                minValue.set(100)
            }
        }
        filters {
            excludes {
                annotatedBy("*Serializable")
                annotatedBy("*Generated")
            }
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = rootProject.name
            from(components["kotlin"])
            // artifact(dokkaJar)
            artifact(sourcesJar)
            artifact(testJar)
            Publish.populatePom(pom)
        }
    }
}
tasks.getByName("publishToMavenLocal").dependsOn("build")

object Publish {
    const val GROUP = "com.pajato"
    private const val POM_DESCRIPTION = "The Pajato Logger project"
    private const val POM_DEVELOPER_ID = "pajatopmr"
    private const val POM_DEVELOPER_NAME = "Paul Michael Reilly"
    private const val POM_ORGANIZATION_NAME = "Pajato Technologies LLC"
    private const val POM_ORGANIZATION_URL = "https://pajato.com/"
    private const val POM_NAME = "pajato-logger-doc"
    private const val POM_LICENSE_DIST = "repo"
    private const val POM_LICENSE_NAME = "The GNU Lesser General Public License, Version 3"
    private const val POM_LICENSE_URL = "https://www.gnu.org/copyleft/lesser.html"
    private const val POM_SCM_CONNECTION = "scm:git:https://gitlab.com/pajato/pajato-logger.git"
    private const val POM_SCM_DEV_CONNECTION = "scm:git:git@gitlab.com:pajato/pajato-logger.git"
    private const val POM_SCM_URL = "https://gitlab.com/pajato/pajato-logger"
    private const val POM_URL = "https://gitlab.com/pajato/pajato-logger"

    fun populatePom(pom: MavenPom) {
        fun populatePomBasic() = pom.apply {
            name.set(POM_NAME)
            description.set(POM_DESCRIPTION)
            url.set(POM_URL)
        }

        fun populatePomLicenses() = pom.apply {
            licenses {
                license {
                    name.set(POM_LICENSE_NAME)
                    url.set(POM_LICENSE_URL)
                    distribution.set(POM_LICENSE_DIST)
                }
            }
        }

        fun populatePomScm() = pom.apply {
            scm {
                url.set(POM_SCM_URL)
                connection.set(POM_SCM_CONNECTION)
                developerConnection.set(POM_SCM_DEV_CONNECTION)
            }
        }

        fun populatePomDevelopers() = pom.apply {
            fun populatePomDevelopers(spec: MavenPomDeveloperSpec) = pom.apply {
                fun populatePomDeveloper(dev: MavenPomDeveloper) = pom.apply {
                    fun populatePomOrganization(org: MavenPomOrganization) {
                        org.name.set(POM_ORGANIZATION_NAME)
                        org.url.set(POM_ORGANIZATION_URL)
                    }

                    dev.id.set(POM_DEVELOPER_ID)
                    dev.name.set(POM_DEVELOPER_NAME)
                    organization { populatePomOrganization(this) }
                }

                spec.developer { populatePomDeveloper(this) }
            }

            developers { populatePomDevelopers(this) }
        }

        populatePomBasic()
        populatePomLicenses()
        populatePomScm()
        populatePomDevelopers()
    }
}

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(project.property("SONATYPE_NEXUS_USERNAME") as String)
            password.set(project.property("SONATYPE_NEXUS_PASSWORD") as String)
        }
    }
}

dependencies {
    implementation(libs.kotlinx.serialization.json)
    implementation(libs.kotlin.test)
    implementation(libs.junit.jupiter)

    testImplementation(libs.kotlinx.coroutines.test)
}
